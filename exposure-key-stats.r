# SPDX-License-Identifier: Apache-2.0

data <- read.table (file="/k/corona/exposure_keys/all_keys.csv", header=FALSE, sep=';')

# count keys per rki start date and per batch
batch_starts <- list(data[[1]])
rki_starts <- list(data[[2]])
users_per_batch <- list()

for (i in 1:length (rki_starts)) {
    count <- table(batch_starts[[1]], rki_starts[[i]])
    users_per_batch[[i]] <- count
}

users_per_batch

# drop first row, that contains garbage
users_per_batch[[1]] <- users_per_batch[[1]][-1,]

# determine unique user counts
for (i in 1:length(users_per_batch[[1]][,1])) {
    cols <- length(users_per_batch[[1]][1,])
    for (j in 0:(cols-2)) {
        users_per_batch[[1]][i, cols - j] <- max(0, (users_per_batch[[1]][i, cols - j] - users_per_batch[[1]][i, cols - j - 1]))
    }
}

# remove key padding
users_per_batch[[1]] <- users_per_batch[[1]] * 0.1

print("unqiue users per batch x first key time")
users_per_batch

print("unique users per batch")
unique_users_per_batch <- rowSums(users_per_batch[[1]])
names(unique_users_per_batch) <- as.POSIXct(as.numeric(names(unique_users_per_batch)), origin = "1970-01-01", tz="UTC")
unique_users_per_batch

print("unique users per first key time")
start_times <- colSums(users_per_batch[[1]])
start_times
names(start_times) <- as.POSIXct(as.numeric(names(start_times)), origin = "1970-01-01", tz="UTC")

print("total counts of unique users")
total_count <- sum(start_times)
total_count

barplot(start_times, xlab="Start time", ylab="Unique Users")


plot(unique_users_per_batch, type="l")
