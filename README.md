# Collection and Aggregation of Temporary Exposure Keys

See https://github.com/corona-warn-app/ for what this is and where this data comes from.

Usage:
- adapt storage location at the top of the `get-exposure-keys.py` script
- run the script
- load the resulting csv file into your favorite data analysis tool

Known Issues:
- the 2020-06-23-8 batch seems to contain questionable data, so you might want to remove that
- key padding is not removed, you need to consider this when analyzing the data (ie. divide all counts by 10)
