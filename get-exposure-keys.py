#!/usr/bin/env python3
# SPDX-License-Identifier: Apache-2.0

import keyExportFormat_pb2

import binascii
import io
import requests
import os
import zipfile

EXPOSURE_KEY_STORAGE = '/k/corona/exposure_keys/'
EXPOSURE_KEY_DB = EXPOSURE_KEY_STORAGE + 'all_keys.csv'

def getKeysForHour(date, hour):
    zipFileName = EXPOSURE_KEY_STORAGE + date + '/' + hour + '/export.zip'
    if os.path.exists(zipFileName):
        return

    os.makedirs(EXPOSURE_KEY_STORAGE + date + '/' + hour)
    req = requests.get('https://svc90.main.px.t-online.de/version/v1/diagnosis-keys/country/DE/date/' + date + '/hour/' + hour)
    zipFile = open(zipFileName, 'wb')
    zipFile.write(req.content)
    zipFile.close()

    zipData = zipfile.ZipFile(zipFileName, 'r')
    binData = zipData.read('export.bin')
    keys = keyExportFormat_pb2.TemporaryExposureKeyExport()
    keys.ParseFromString(binData[16:])

    if keys.end_timestamp - keys.start_timestamp != 3600:
        print('Warning: batch sizes differs from 1 hour!')
    # TODO check the batch start time always matches date-hour

    keyDb = open(EXPOSURE_KEY_DB, 'a')
    for key in keys.keys:
        keyDb.write('{batch_start};{rolling_start_interval_number};{rolling_period};{transmission_risk_level};{key_data}\n'.format(
            batch_start = keys.start_timestamp,
            rolling_start_interval_number = key.rolling_start_interval_number * 600,
            rolling_period = key.rolling_period * 600,
            transmission_risk_level = key.transmission_risk_level,
            key_data = binascii.hexlify(key.key_data)
        ));
    keyDb.close()

def listHours(date):
    req = requests.get('https://svc90.main.px.t-online.de/version/v1/diagnosis-keys/country/DE/date/' + date + '/hour')
    for hour in req.json():
        getKeysForHour(date, str(hour))

def listDates():
    req = requests.get('https://svc90.main.px.t-online.de/version/v1/diagnosis-keys/country/DE/date')
    for date in req.json():
        listHours(date)

listDates()
