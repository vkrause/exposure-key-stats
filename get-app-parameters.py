#!/usr/bin/env python3
# SPDX-License-Identifier: Apache-2.0

import applicationConfiguration_pb2

import datetime
import io
import os
import requests
import zipfile

EXPOSURE_CONFIG_STORAGE = '/k/corona/app_config/'

def downloadLatest():
    if not os.path.exists(EXPOSURE_CONFIG_STORAGE):
        os.makedirs(EXPOSURE_CONFIG_STORAGE)
    appConfigFile = EXPOSURE_CONFIG_STORAGE + datetime.date.today().isoformat() + ".zip"
    if os.path.exists(appConfigFile):
        return
    req = requests.get('https://svc90.main.px.t-online.de/version/v1/configuration/country/DE/app_config')
    f = open(appConfigFile, 'wb')
    f.write(req.content)
    f.close()

    zipFile = zipfile.ZipFile(appConfigFile, 'r')
    binData = zipFile.read('export.bin')

    appConfig = applicationConfiguration_pb2.ApplicationConfiguration()
    appConfig.ParseFromString(binData)

    print(appConfig)


downloadLatest()
